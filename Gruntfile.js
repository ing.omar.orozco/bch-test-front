module.exports = function(grunt) {

    var gruntBase = require('./base/grunt.base.js');
    var buildConfig = require('./base/build.config.js');

    buildConfig.context = 'www/web/formulario';
    buildConfig.commonContext = 'www/web';

    grunt.initConfig(grunt.util._.extend(gruntBase.getConfig(grunt), buildConfig));
    gruntBase.init(grunt);
};
