(function() {
    angular
        .module('WebBancoEvaluacion')
        .run(appRun);
    appRun.$inject = ['$rootScope'];
    function appRun($rootScope) {
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
            if (error!=null && error.code !=null) {
                console.log("error",error.code);
            }
        });
        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            if (angular.isDefined(toState.data.pageTitle)) {
                $rootScope.pageTitle = toState.data.pageTitle + ' | Banco de Chile';
            }
        });
    }
})();
