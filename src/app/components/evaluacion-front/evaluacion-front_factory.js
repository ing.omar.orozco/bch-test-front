(function () {
    angular.module('WebBancoEvaluacion.evaluacionFront')
    .factory('evaluacionFrontFactory', evaluacionFrontFactory);

    evaluacionFrontFactory.$inject = ['$http'];

    function evaluacionFrontFactory($http) {
      
        return {
            getPosts: getPosts
        };

        function getPosts() {
            var urlConsulta = "https://jsonplaceholder.typicode.com/posts";
            return $http.get(urlConsulta);
        }

      
    }
})();