(function () {
    

    angular
        .module('WebBancoEvaluacion.evaluacionFront')
        .constant('FILE_TYPE_INVALIDATE', {
            tipo: 'E',
            cuerpo: {
                message: 'El tipo de archivo no esta permitido',
                title: ''
            },
            servicio: null
        });
})();
