(function () {
    angular.module('WebBancoEvaluacion.evaluacionFront').config(evaluacionFrontConfig);
    function evaluacionFrontConfig($stateProvider) {
      $stateProvider.state('evaluacion-front', {
        parent: 'root',
        url: '/evaluacion-front',
        views: {
          "main@": {
            controller: 'evaluacionFrontCtrl',
            templateUrl: 'components/evaluacion-front/view/evaluacion-front_tpl.html',
            controllerAs: 'vm'
          }
        },
        data: {
          pageTitle: 'Evaluacion Front'
        }
      });
    }
  })();