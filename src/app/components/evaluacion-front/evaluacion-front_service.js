(function () {
    angular.module('WebBancoEvaluacion.evaluacionFront')
    .service('evaluacionFrontService', evaluacionFrontService);

    evaluacionFrontService.$injector = ['$injector'];

    function evaluacionFrontService($injector) {
        var serv = this;
        serv.$q = $injector.get('$q');
        serv.$filter = $injector.get('$filter');
        serv.evaluacionFrontFactory = $injector.get('evaluacionFrontFactory');
  
    }

    evaluacionFrontService.prototype.getPosts = function () {
        var serv = this;
       var deferred = serv.$q.defer();
       serv.evaluacionFrontFactory.getPosts().then(function (response) {
        return deferred.resolve(response.data);   
       }, function (err) { return deferred.reject(err);});
       return deferred.promise;
    };
})();