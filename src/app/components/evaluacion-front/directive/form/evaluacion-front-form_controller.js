(function () {
    angular
        .module('WebBancoEvaluacion.evaluacionFront')
        .controller('formEvaluacionFrontCtrl', formEvaluacionFrontCtrl);

        formEvaluacionFrontCtrl.$inject = ['$injector','$scope','$rootScope'];
    function formEvaluacionFrontCtrl($injector,$scope,$rootScope) {
        var vm = this;
        
        vm.RestService = $injector.get("evaluacionFrontService");
        vm.resultGetPost = null;
        vm.descripcion="";
        vm.titulo = "";
        vm.id_usuario = "";
        vm.init();
      
      }
    
      formEvaluacionFrontCtrl.prototype.init = function () {
        var vm = this;
      };
    
      formEvaluacionFrontCtrl.prototype.iniciar = function () {
        var vm = this;
        vm.RestService.getPosts().then(function (response) {
          vm.$scope.posts = response;
          vm.$scope.activebtn = false;
          vm.$scope.activelist = true;
          vm.$scope.activeform = false;
        }, function () { 
              console.log("error");
        });
      };
    
    
      formEvaluacionFrontCtrl.prototype.guardar = function () {
        var vm = this;
        var ultimoValor = 0;
        angular.forEach(vm.$scope.posts, function (m) {
            ultimoValor = m.id;
        });
        if(vm.id_usuario === "" && vm.titulo === "" && vm.descripcion === ""){
           // agregar modal
        }else{
            vm.$scope.posts.push({
                "userId": vm.id_usuario,
                "id": ultimoValor + 1,
                "title": vm.titulo,
                "body": vm.descripcion
              });
            vm.guardarExito();
        }
        
      };

      formEvaluacionFrontCtrl.prototype.guardarExito = function () {
        var vm = this;
        vm.id_usuario = "";
        vm.titulo = "";
        vm.descripcion = "";
        vm.$scope.activebtn = false;
        vm.$scope.activelist = true;
        vm.$scope.activeform = false;
      };
 
})();