(function () {
    angular
        .module('WebBancoEvaluacion.evaluacionFront')
        .directive('cdnFormEvaluacionFront', formEvaluacionFrontDirective);


    /* @ngInject */
    function formEvaluacionFrontDirective() {
        return {
            scope: {
                accion: '=',
                model: '=',
                modelToEdit: '=',
                userProfile: '='
            },
            restrict: 'EA',
            controllerAs: 'vm',
            templateUrl: 'components/evaluacion-front/directive/form/evaluacion-front-form_tpl.html',
            controller: 'formEvaluacionFrontCtrl',
            bindToController: true
        };
    }
})();
