(function () {
  angular
    .module('WebBancoEvaluacion', [
      'ui.router',
      'ui.router.state.events',
      'ui.bootstrap',
      'ui.utils',
      'ui.select',
      'ngAnimate',
      'templates-app',
      'templates-taurus',
      'angular-loading-bar',
      'WebBancoEvaluacion.evaluacionFront'
    ]);
})();