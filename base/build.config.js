/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {
    /**
     * The `build_dir` folder is where our projects are compiled during
     * development and the `compile_dir` folder is where our app resides once it's
     * completely built.
     */
    build_dir: 'build',
    compile_dir: 'bin',
    src_dir: 'src',
    sonar_tmp_dir: '.tmp',
    karma_reports: 'reports',
    build_zip: 'build.zip',
    bin_zip: 'bin.zip',

    /**
     * This is a collection of file patterns that refer to our app code (the
     * stuff in `src/`). These file paths are used in the configuration of
     * build tasks. `js` is all project javascript, less tests. `common_tpl` contains
     * our reusable components' (`src/common`) template HTML files, while
     * `app_tpl` contains the same, but for our app's code. `html` is just our
     * main HTML file, `less` is our main stylesheet, and `unit` contains our
     * app's unit tests.
     */
    app_files: {
        modjs: ['src/**/*_module.js'],
        configjs: ['src/**/*_config.js'],
        js: [
            'src/**/*.js',
            '!src/**/*_test.js',
            '!src/assets/**/*.js',
            '!src/assets/**/*_config.js',
            '!src/assets/**/*_module.js'
        ],
        jsunit: ['src/**/*_test.js'],

        app_tpl: ['src/app/**/*_tpl.html'],
        orion_tpl: ['vendor/orion-*/**/*.tpl.html'],
        taurus_tpl: ['vendor/taurus-*/**/*_tpl.html'],
        common_tpl: ['src/common/**/*_tpl.html'],
        taurus_files: ['vendor/taurus-*/src/app/**/*.js', 'vendor/taurus-*/**/*.html'],

        html: ['src/index.html'],
        less: 'src/less/main.less',
        stylus_folder: 'src/stylus/',
        stylus_component_folder: 'src/app/components/**/',
        stylus_vendor_orion_folders: 'vendor/orion-*/stylus/',
        stylus_vendor_taurus_folders: 'vendor/taurus-*/**/stylus/',
        stylus_style_guide: 'vendor/taurus-style-guide/src/stylus/',
        taurus_header: 'vendor/taurus-header/src/app/components/header/'

    },

    /**
     * This is a collection of files used during testing only.
     */
    test_files: {
        js: [
            'vendor/angular-mocks/angular-mocks.js'
        ]
    },

    /**
     * This is the same as `app_files`, except it contains patterns that
     * reference vendor code (`vendor/`) that we need to place into the build
     * process somewhere. While the `app_files` property ensures all
     * standardized files are collected for compilation, it is the user's job
     * to ensure non-standardized (i.e. vendor-related) files are handled
     * appropriately in `vendor_files.js`.
     *
     * The `vendor_files.js` property holds files to be automatically
     * concatenated and minified with our project source files.
     *
     * The `vendor_files.css` property holds any CSS files to be automatically
     * included in our app.
     *
     * The `vendor_files.assets` property holds any assets to be copied along
     * with our app's assets. This structure is flattened, so it is not
     * recommended that you use wildcards.
     */
    vendor_files: {
        js: [
            'vendor/jquery/dist/jquery.js',
            'vendor/angular/angular.js',
            'vendor/angular-atmosphere/app/scripts/services/angular-atmosphere.js',
            'vendor/angular-atmosphere-service/service/angular-atmosphere-service.js',
            'vendor/atmosphere.js/atmosphere.js',
            'vendor/angular-local-storage/dist/angular-local-storage.js',
            'vendor/angular-loading-bar/build/loading-bar.min.js',
            'vendor/angular-input-masks/angular-input-masks-standalone.js',
            'vendor/angular-toastr/dist/angular-toastr.tpls.js',
            'vendor/angular-float-thead/angular-floatThead.js',
            'vendor/angular-sweetalert/SweetAlert.js',
            'vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
            'vendor/angular-clipboard/angular-clipboard.js',
            'vendor/angular-ui-router/release/angular-ui-router.js',
            'vendor/angular-ui-router/release/stateEvents.js',
            'vendor/angular-file-saver/dist/angular-file-saver.bundle.js',
            'vendor/angular-ui-utils/modules/route/route.js',
            'vendor/angular-file-upload/angular-file-upload.js',
            'vendor/angular-i18n/angular-locale_es-cl.js',
            'vendor/angular-messages/angular-messages.js',
            'vendor/angular-rut/dist/angular-rut.js',
            'vendor/angular-ui-utils/ui-utils.js',
            'vendor/angular-sanitize/angular-sanitize.js',
            'vendor/angular-xeditable/dist/js/xeditable.js',
            'vendor/angular-tablesort/js/angular-tablesort.js',
            'vendor/angular-resource/angular-resource.js',
            'vendor/angular-ui-select/dist/select.js',
            'vendor/angular-animate/angular-animate.js',
            'vendor/angular-rangeslider/angular.rangeSlider.js',
            'vendor/angular-ladda/dist/angular-ladda.min.js',
            'vendor/angular-progress-button-styles/dist/angular-progress-button-styles.min.js',
            'vendor/angular-ui-grid/ui-grid.js',
            'vendor/checklist-model/checklist-model.js',
            'vendor/clockpicker/dist/bootstrap-clockpicker.js',
            'vendor/clockpicker/dist/jquery-clockpicker.js',
            'vendor/dndLists/angular-drag-and-drop-lists.min.js',
            'vendor/modernizr/modernizr.js',
            'vendor/lodash/dist/lodash.js',
            'vendor/orion-*/**/*.module.js',
            'vendor/orion-*/**/*.js',
            'vendor/ladda/js/spin.js',
            'vendor/file-saver.js/FileSaver.js',
            'vendor/ladda/js/ladda.js',
            'vendor/ng-scrollbar/dist/ng-scrollbar.js',
            'vendor/ng-caps-lock/ng-caps-lock.js',
            'vendor/highcharts/highcharts.src.js',
            'vendor/highcharts-ng/dist/highcharts-ng.js',
            'vendor/sweetalert-ui/lib/sweet-alert.js',
            'vendor/floatThead/dist/jquery.floatThead.min.js',
            'vendor/ng-sticky-element/dist/ng-sticky-element.min.js',
            'vendor/moment/moment.js',
            'vendor/angular-translate/angular-translate.js',
            'vendor/angular-translate-loader-partial/angular-translate-loader-partial.js',
            'vendor/angular-off-click/dist/angular-off-click.js',
            'vendor/orion-directivas-tablas/src/orionTablas/directives/orionTablas.js',
            'vendor/orion-directivas-tablas/src/orionTablas/directives/orionTablaSelect.js',
            'vendor/orion-directivas-tablas/src/orionTablas/orionTablas.constants.js',
            'vendor/orion-bch-*/**/*.js',
            'vendor/orion-directive-*/**/*.js',
            'vendor/crypto-js/crypto-js.js',
            'vendor/ng-file-upload*/ng-file-upload*.js',
            'vendor/angular-click-outside/clickoutside.directive.js',
            'vendor/pdfmake/build/pdfmake.js',
            'vendor/pdfmake/build/vfs_fonts.js',
            'vendor/html2canvas/build/html2canvas.js',
            'vendor/angular-svg-round-progressbar/build/roundProgress.min.js',
            'vendor/bower-canvas2image/canvas2image/canvas2image.js',
            'vendor/angularjs-gauge/src/angularjs-gauge.js'
        ],
        json: [
            'vendor/taurus-catalogos/src/**/*.json'
        ],
        css: [
            'vendor/bootstrap/dist/css/bootstrap.min.css',
            'vendor/angular-ui-select/dist/select.min.css',
            'vendor/ng-scrollbar/dist/ng-scrollbar.min.css',
            'vendor/ionicons/css/ionicons.min.css',
            'vendor/ladda/dist/ladda-themeless.min.css',
            'vendor/sweetalert-ui/lib/sweet-alert.css',
            'vendor/clockpicker/dist/bootstrap-clockpicker.min.css',
            'vendor/clockpicker/dist/jquery-clockpicker.min.css',
            'vendor/ng-sticky-element/dist/ng-sticky-element.min.css',
            'vendor/font-awesome/css/font-awesome.min.css',
            'vendor/angular-loading-bar/build/loading-bar.min.css',
            'vendor/angular-toastr/dist/angular-toastr.min.css',
            'vendor/angular-progress-button-styles/dist/angular-progress-button-styles.min.css',
            'vendor/angular-xeditable/dist/css/xeditable.min.css',
            'vendor/angular-ui-grid/ui-grid.min.css'
        ],
        assets: [
            'vendor/orion-*/assets/**',
            'src/assets/**'
        ],
        fonts: [
            'vendor/ionicons/fonts/ionicons.ttf',
            'vendor/orion-ui-grid/ui-grid.ttf',
            'vendor/font-awesome/fonts/fontawesome-webfont.ttf',
            'vendor/bootstrap/fonts/glyphicons-halflings-regular.ttf'
        ]
    }
};
