(function() {
    "use strict"; //enable ECMAScript 5 Strict Mode

    function filterForJS(files) {
        return files.filter(function(file) {
            return file.match(/\.js$/);
        });
    }

    function filterForCSS(files) {
        return files.filter(function(file) {
            return file.match(/\.css$/);
        });
    }

    function init(grunt) {
        grunt.loadNpmTasks('grunt-contrib-clean');
        grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-jshint');
        grunt.loadNpmTasks('grunt-contrib-concat');
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-conventional-changelog');
        grunt.loadNpmTasks('grunt-bump');
        grunt.loadNpmTasks('grunt-karma');
        grunt.loadNpmTasks('grunt-ngmin');
        grunt.loadNpmTasks('grunt-html2js');
        grunt.loadNpmTasks('grunt-contrib-stylus');
        grunt.loadNpmTasks('grunt-string-replace');
        grunt.loadNpmTasks('grunt-browser-sync');
        grunt.loadNpmTasks('grunt-express-server');
        grunt.loadNpmTasks('grunt-zip');
        grunt.loadNpmTasks('grunt-karma-sonar');

        grunt.registerMultiTask('index', 'Process index.html template', function() {
            var dirRE = new RegExp('^(' + grunt.config('build_dir') + '|' + grunt.config('compile_dir') + ')\/', 'g');
            var jsFiles = filterForJS(this.filesSrc).map(function(file) {
                return file.replace(dirRE, '');
            });
            var cssFiles = filterForCSS(this.filesSrc).map(function(file) {
                return file.replace(dirRE, '');
            });

            grunt.file.copy('src/index.html', this.data.dir + '/index.html', {
                process: function(contents, path) {
                    return grunt.template.process(contents, {
                        data: {
                            scripts: jsFiles,
                            styles: cssFiles,
                            version: grunt.config('pkg.version')
                        }
                    });
                }
            });
        });

        grunt.registerMultiTask('karmaconfig', 'Process karma config templates', function() {
            var jsFiles = filterForJS(this.filesSrc);

            grunt.file.copy('karma/karma-unit.tpl.js', grunt.config('build_dir') + '/karma-unit.js', {
                process: function(contents, path) {
                    return grunt.template.process(contents, {
                        data: {
                            scripts: jsFiles
                        }
                    });
                }
            });
        });

        grunt.registerTask('prepare', ['clean']);
        grunt.registerTask('test', ['jshint']);
        grunt.registerTask('compile', ['html2js', 'stylus', 'string-replace', 'concat', 'uglify', 'copy:all', 'copy:css','copy:build_vendor_assets', 'index', 'copy:context']);
        grunt.registerTask('continuous', ['karmaSonar']);
        grunt.registerTask('package', ['zip']);

        grunt.registerTask('default', ['prepare', 'test', 'compile', 'package']);

        grunt.registerTask('build', 'Develop Environment', function() {
            grunt.config.set('uglify.compile_taurus.options.compress', false);
            grunt.config.set('uglify.compile_taurus.options.beautify', true);
            grunt.config.set('uglify.compile_app.options.compress', false);
            grunt.config.set('uglify.compile_app.options.beautify', true);
            grunt.task.run('default');
        });

        grunt.registerTask('sonar', ['prepare', 'test', 'compile', 'continuous', 'package']);

        grunt.registerTask('minibuild', [
            'jshint',
            'html2js:app',
            'html2js:common',
            'string-replace:single_file',
            'concat:compile_appjs',
            'concat:compile_htmljs',
            'copy:all',
            'copy:build_vendor_assets',
            'index',
            'copy:context',
            'zip'
        ]);
        grunt.registerTask('ux', [
            'html2js:app',
            'html2js:common',
            'stylus',
            'string-replace:single_file',
            'concat:compile_htmljs',
            'copy:css',
            'index',
            'copy:context'
        ]);
    }

    function getConfig(grunt, options) {

        var isBamboo = grunt.option("bamboo");
        var sonardata = {
            host: undefined,
            jdbc: undefined,
            user: undefined,
            password: undefined
        };
        if (isBamboo) {
            sonardata = grunt.file.readJSON("/home/bamboo/sonardata.json");
        }

        return {

            pkg: grunt.file.readJSON("package.json"),

            meta: { banner: '/**\n' + ' * <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' + ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' + ' */\n' },

            watch: {
                all:{
                    files: ['src/app/**/*.js', 'src/app/**/*.html'],
                    tasks: ["minibuild"]
                }
            },

            //PREPARE
            clean: {
                all: [
                    '<%= build_dir %>',
                    '<%= compile_dir %>',
                    '<%= karma_reports %>',
                    '<%= sonar_tmp_dir %>',
                    '<%= bin_zip %>',
                    '<%= build_zip %>',
                    '<%= context %>'
                ]
            },

            //TEST
            jshint: {
                options: {
                    curly: true,
                    newcap: true,
                    noarg: true,
                    sub: true,
                    boss: true,
                    eqnull: true
                },
                all: ['<%= app_files.js %>']
            },

            //COMPILE
            html2js: {
                app: {
                    options: {
                        base: 'src/app'
                    },
                    src: ['<%= app_files.app_tpl %>'],
                    dest: '<%= build_dir %>/templates-app.js'
                },
                orion: {
                    options: {
                        base: 'vendor'
                    },
                    src: ['<%= app_files.orion_tpl %>'],
                    dest: '<%= build_dir %>/templates-orion.js'
                },
                taurus: {
                    options: {
                        base: 'vendor'
                    },
                    src: ['<%= app_files.taurus_tpl %>'],
                    dest: '<%= build_dir %>/templates-taurus.js'
                },
                common: {
                    options: {
                        base: 'src/common'
                    },
                    src: ['<%= app_files.common_tpl %>'],
                    dest: '<%= build_dir %>/templates-common.js'
                }
            },

            'string-replace': {
                stylus: {
                    files: {
                        '<%= app_files.stylus_style_guide %>main.styl': '<%= app_files.stylus_style_guide %>main.styl'
                    },
                    options: {
                        replacements: [{
                            pattern: "@import '../../node_modules/jeet/stylus/jeet/index'",
                            replacement: "@import '../../../../node_modules/jeet/stylus/jeet/index'"
                        }]
                    }
                },
                single_file: {
                    files: {
                        '<%= build_dir %>/templates-orion.js': '<%= build_dir %>/templates-orion.js',
                        '<%= build_dir %>/templates-taurus.js': '<%= build_dir %>/templates-taurus.js'
                    },
                    options: {
                        replacements: [{
                            pattern: /orion-/g,
                            replacement: ''
                        }, {
                            pattern: /taurus-([A-Za-z0-9-]*)\/src\/app\//g,
                            replacement: ''
                        }]
                    }
                }
            },

            concat: {
                compile_appjs: {
                    options: { banner: '<%= meta.banner %>' },
                    src: [
                        '<%= app_files.modjs %>',
                        '<%= app_files.configjs %>',
                        '<%= app_files.js %>'
                    ],
                    dest: '<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>_app.js'
                },
                compile_htmljs: {
                    options: { banner: '<%= meta.banner %>' },
                    src: [
                        '<%= html2js.app.dest %>',
                        '<%= html2js.orion.dest %>',
                        '<%= html2js.taurus.dest %>',
                        '<%= html2js.common.dest %>'
                    ],
                    dest: '<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>_html.js'
                },
                compile_vendor: {
                    src: ['<%= vendor_files.js %>'],
                    dest: '<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>_vendor.js'
                }
            },

            uglify: {
                compile_app: {
			options: { banner: '<%= meta.banner %>', mangle: false , sourceMap:'<%= concat.compile_appjs.dest %>.map'},
                    files: { '<%= concat.compile_appjs.dest %>': '<%= concat.compile_appjs.dest %>' }
                },
                compile_vendor: {
			options: { banner: '<%= meta.banner %>', mangle: false , sourceMap:'<%= concat.compile_vendor.dest %>.map'},
                    files: { '<%= concat.compile_vendor.dest %>': '<%= concat.compile_vendor.dest %>' }
                },
		compile_html2js: {
			options: { banner: '<%= meta.banner %>', mangle: false , sourceMap:'<%= concat.compile_htmljs.dest %>.map'},
                    files: { '<%= concat.compile_htmljs.dest %>': '<%= concat.compile_htmljs.dest %>'  }
	        }
            },

            stylus: {
                all: {
                    options: {
                        paths: ['.'],
                        import: ['vendor/orion-guideline-personas/stylus/parciales/variables.styl']
                    },
                    files: {
                        '<%= build_dir %>/assets/<%= pkg.name %>--<%= pkg.version %>.css': [
                            '<%= app_files.stylus_folder %>*.styl',
                            '<%= app_files.stylus_component_folder %>*.styl',
                            '<%= app_files.stylus_style_guide %>*.styl',
                            '<%= app_files.stylus_vendor_orion_folders %>*.styl',
                            '<%= app_files.stylus_vendor_taurus_folders %>*.styl'
                        ]
                    }
                }
            },

            copy: {
                all: {
                    files: [{
                        src: [
                            '<%= vendor_files.json %>',
                            '<%= vendor_files.css %>',
                            '<%= vendor_files.fonts %>'
                        ],
                        dest: '<%= compile_dir %>/',
                        cwd: '.',
                        expand: true
                    }]
                },
                css: {
                    files: [{
                        src: ['assets/<%= pkg.name %>--<%= pkg.version %>.css'],
                        dest: '<%= compile_dir %>/',
                        cwd: '<%= build_dir %>',
                        expand: true
                    }]
                },
                build_vendor_assets: {
                    files: [{
                        src: '<%= vendor_files.assets %>',
                        dest: '<%= compile_dir %>/assets/',
                        cwd: '.',
                        expand: true,
                        filter: 'isFile',
                        rename: function(dest, src) {
                            var path = require('path');
                            var component = src.split("/")[1];
                            var dir_relativo = src.split("/assets/")[1];
                            return path.join(dest, dir_relativo);
                        }
                    }]
                },
                context: {
                    files: [{
                        src: ['**/*.*'],
                        dest: '<%= context %>',
                        cwd: './<%= compile_dir %>',
                        expand: true
                    }]
                },

                properties: {
                    files: [{
                        src: ['**/*.json'],
                        dest: '<%= commonContext %>',
                        cwd: './<%= src_dir %>/assets/properties',
                        expand: true,
                        filter: function (filepath) {
                            var path = require('path');
                            var dest = path.join(
                                grunt.config('commonContext'),
                                path.basename(filepath)
                            );
                            return !(grunt.file.exists(dest));
                        } 
                    }]
                }
            },

            index: {
                build: {
                    dir: '<%= compile_dir %>',
                    src: [
                        '<%= vendor_files.css %>',
                        '<%= concat.compile_vendor.dest %>',
                        '<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>_html.js',
                        '<%= build_dir %>/assets/<%= pkg.name %>--<%= pkg.version %>.css',
                        '<%= concat.compile_appjs.dest %>'
                    ]
                }
            },

            //PACKAGE
            zip: { '<%= bin_zip %>': ['bin/**','!bin/**/*.map'], '<%= build_zip %>': ['bin/**'] },

            //CONTINUOUS
            karmaSonar: {
                options: {
                    instance: {
                        hostUrl: sonardata.host,
                        jdbcUrl: sonardata.jdbc,
                        jdbcUsername: sonardata.user,
                        jdbcPassword: sonardata.password
                    }
                },
                sonarqube: {
                    project: {
                        key: 'cl.bancochile.taurus.<%= pkg.name %>:front',
                        name: '<%= pkg.name %>',
                        version: '<%= pkg.version %>'
                    },
                    paths: [{
                        cwd: '.', // the current working directory'
                        src: 'src', // the source directory within the cwd
                        test: 'src', // the test directory within the cwd
                        reports: {
                            unit: 'reports/TESTS-xunit.xml', // the result file within the cwd
                            coverage: 'reports/coverage/lcov.info' // the glob for lcov files'
                        }
                    }]
                }
            },

            karmaconfig: {
                unit: {
                    dir: '<%= build_dir %>',
                    src: [
                        '<%= vendor_files.js %>',
                        '<%= html2js.app.dest %>',
                        '<%= html2js.orion.dest %>',
                        '<%= html2js.taurus.dest %>',
                        '<%= html2js.common.dest %>',
                        '<%= test_files.js %>'
                    ]
                }
            },

            karma: {
                options: {
                    configFile: '<%= build_dir %>/karma-unit.js'
                },
                unit: {
                    port: 9019,
                    background: true
                },
                continuous: {
                    singleRun: true,
                    captureConsole: true
                }
            }

        };
    }

    exports = module.exports = {
        init: init,
        getConfig: getConfig
    };

})();
