_**EVALUACION FRONT**_

**PARA CLONAR EL PROYECTO USE GIT**
- git clone https://gitlab.com/ing.omar.orozco/bch-test-front.git

**PASOS PARA INSTALAR Y COMPILAR**
1.  EJECUTE npm install
1.  EJECUTE bower install
1.  EJECUTE grunt build

 

** PARA EJECUTAR O LEVANTAR EL PROYECTO**
 - use npm start
-  esta configurado para levantar en el puerto 8006
-  acceda por la siguiente url
-  http://localhost:8006/web/formulario/#/evaluacion-front
