/**
 * Node Configuration
 *
 * @author Banco de Chile
 *
 */

var colors = require('colors');
var httpProxy = require('http-proxy');
var connect = require('connect');
var open =  require('open');
var http = require('http');
var isQA = true;

var options = {
    'static': 'http://localhost:3000',
    'local': 'http://localhost:7001',
    'mock': 'http://localhost:8080',
};


var proxy = httpProxy.createProxyServer({});

var proxyApp = connect().use(function(req,res) {

    var target = options.static;

    var contexto = getContexto(req.url);
    
    proxy.web(req, res, {
        target: target
    });
});

http.createServer(proxyApp).listen(8006);
console.info('Running HTTP Proxy');

var app = connect()
    .use(connect.static('www/'))
    .use(connect.directory('www/'))
    .use(connect.cookieParser())
    .use(connect.session({ secret: 'my secret here' }));

http.createServer(app).listen(3000, openBrowser);


function openBrowser(){
    console.log('Open browser'.green);
    console.log('Running Web Server'.green);
}

function getContexto(urlConsulta) {
    var indexFin = urlConsulta.substr(1).indexOf("/");
    return urlConsulta.substr(1,indexFin);
}
